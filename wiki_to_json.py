#!/usr/bin/env python3
#
#    wiki_to_json.py - Wiki to json exporter to for COBHUNI project
#
#    Copyright (C) 2016  Alicia González Martínez, aliciagm85+code@gmail.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
#
# dependencies:
#   * exporthandler.py
#   * wikiparser.py
#   * offsetsbuilder.py
#   * util.py
#   * config.ini
#                                +------------------+        +---------------+
#   +-----------------+          |                  | -----> |               |
#   |                 | -------> | exporthandler.py |   str  | wikiparser.py |
#   |                 | <------- |                  | <----- |               |
#   |                 |   json   +------------------+  json  +---------------+
#   | wiki_to_json.py |          
#   |                 |          +-------------------+
#   |                 | -------> |                   |
#   |                 |  json    | offsetsbuilder.py | ----> json output files
#   +-----------------+          |                   |
#                                +-------------------+        
# example:
#   $ python wiki_to_json.py --title 1_altaufi.djvu
#
################################################################################

import os
import sys
import json
import argparse
from configparser import ConfigParser

CURRENT_PATH = os.path.dirname(os.path.realpath(__file__))

# append current directory to sys.path
sys.path.insert(0, CURRENT_PATH)

from exporthandler.exporthandler import ExportHandler
from offsetsbuilder.offsetsbuilder import OffsetsBuilder

config = ConfigParser(inline_comment_prefixes=('%'))
config.read(os.path.join(CURRENT_PATH, 'config.ini'))

OUTPATH = config.get('json output', 'path')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Download scan transliterations from the wiki and and dumps them into json.')
    parser.add_argument('--title', help='download only the texts belonging to this title')
    parser.add_argument('--metadata', default='metadata.json', help='file to write metadata [default metadata.json]')
    parser.add_argument('--debug', action='store_true', help='debug mode')
    args = parser.parse_args()

    handler = ExportHandler()
    
    data = handler.export(args.metadata, args.title, args.debug) 

    for scan in json.loads(data):

        #if not scan: continue #FIXME

        fname, fext = os.path.splitext(scan['title'])

        #if fname == '103_tatay_arbain_nawawiya.djvu': #FIXME
        #    continue #FIXME

        with open(os.path.join(OUTPATH, fname+'.json'), 'w') as fp:
            offbr = OffsetsBuilder(json.dumps(scan['content']))
            out = offbr.build()
            fp.write(out)
